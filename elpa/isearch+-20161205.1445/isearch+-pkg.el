;;; -*- no-byte-compile: t -*-
(define-package "isearch+" "20161205.1445" "Extensions to `isearch.el' (incremental search)." 'nil :url "http://www.emacswiki.org/isearch+.el" :keywords '("help" "matching" "internal" "local"))
